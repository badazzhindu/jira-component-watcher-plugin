/* Copyright (c) 2008, 2009, Ray Barham
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Ray Barham ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Ray Barham BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.burningcode.jira.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericEntityException;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentComparator;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;

/**
 * Class used to display all the watchers user and group watchers for a component.
 * 
 * This class returns all the components for the project and the components user and group watchers.
 * It is used to facilitate displaying information on the velocity template JIRAComponentWatcher.vm.
 * 
 * @author Ray Barham
 * @see JiraWebActionSupport
 */
public class JIRAComponentWatcher extends JiraWebActionSupport
{
    private static final long serialVersionUID = -5750941171220722700L;
    private Long pid;
    private JIRAComponentUserWatcher componentUserWatcher;
    private JIRAComponentGroupWatcher componentGroupWatcher;

    /**
     * Default constructor for the class
     */
    public JIRAComponentWatcher()
    {
        try
        {
            this.componentUserWatcher = new JIRAComponentUserWatcher(ComponentAccessor.getUserManager());
            this.componentGroupWatcher = new JIRAComponentGroupWatcher(ComponentAccessor.getGroupManager());
        }

        catch (GenericEntityException e)
        {
            this.addErrorMessage(e.getMessage());
        }
    }

    /**
     * Default action performed.
     * 
     * @return The status of the actions.  Returns "securitybreach" if user does not a project or jira admin.
     * @throws Exception
     */
    public String doDefault() throws Exception
    {
        if (hasProjectAdminPermission() || hasAdminPermission())
            return super.doDefault();
        else
            return "securitybreach";
    }

    /**
     * Execute action.
     * 
     * @return The status of the actions.  Returns "securitybreach" if user does not a project or jira admin.
     * @throws Exception
     */
    protected String doExecute() throws Exception
    {
        if (hasProjectAdminPermission() || hasAdminPermission())
            return super.doExecute();
        else
            return "securitybreach";
    }
    
    /**
     * Returns a list of all users, even those in groups, that are watching this component with the passed id.
     * 
     * @param componentId
     * @return A collection of all users watching.
     */
    public Collection<User> getAllUsersWatching(Long componentId)
    {
        Collection<User> userWatchers = this.getUserWatchers(componentId);
        Collection<User> groupWatchers = ComponentManager.getInstance().getUserUtil().getAllUsersInGroups(this.getGroupWatchers(componentId));
        
        for(Iterator<User> i = groupWatchers.iterator(); i.hasNext();)
        {
        	User user = (User)i.next();
    		if(!userWatchers.contains(user))
    			userWatchers.add(user);
        }
        
        return userWatchers;
    }

    /**
     * Returns a sorted list of all the components for the project Id.
     * 
     * @return List of components.
     */
    public Collection<ProjectComponent> getComponents()
    {
        Collection<ProjectComponent> components = ComponentManager.getInstance().getProjectComponentManager().findAllForProject(this.getProjectId());
        Collections.sort((List<ProjectComponent>)components, ProjectComponentComparator.INSTANCE);

        return components;
    }

    /**
     * Returns a list of all the groups watching a component.
     * 
     * @param componentId Id of the component to get watchers for.
     * @return List of watchers.
     */
    public Collection<Group> getGroupWatchers(Long componentId)
    {
        this.componentGroupWatcher.setComponentId(componentId);
        Collection<Group> objects = this.componentGroupWatcher.getWatchers();
        ArrayList<Group> groupObjects = new ArrayList<Group>();
        for(Iterator<Group> i = objects.iterator(); i.hasNext();)
        {
        	groupObjects.add((Group)i.next());
        }
        return groupObjects;
    }

    /**
     * Returns a Project object of the project being accessed
     * 
     * @return Project object.
     * @throws DataAccessException 
     */
    public Project getProject() throws DataAccessException
    {
        return ComponentManager.getInstance().getProjectManager().getProjectObj(this.pid);
    }

    /**
     * Returns the project ID of the project being accessed
     * 
     * @return The ID of the project.
     */
    public Long getProjectId()
    {
        return this.pid;
    }

    /**
     * Returns a StringUtils object used by the velocity template.
     * 
     * Note: This was only added because the stringUtils paramater did not seem available already.
     * 
     * @return StringUtils object.
     */
    public StringUtils getStringUtils()
    {
        return new StringUtils();
    }

    /**
     * Returns a list of all the users watching a component.
     * 
     * @param componentId Id of the component to get watchers for.
     * @return List of watchers.
     */
    public Collection<User> getUserWatchers(Long componentId)
    {
        this.componentUserWatcher.setComponentId(componentId);
        Collection<User> objects = this.componentUserWatcher.getWatchers();
        ArrayList<User> userObjects = new ArrayList<User>();
        for(Iterator<User> i = objects.iterator(); i.hasNext();)
        {
        	userObjects.add((User)i.next());
        }
        return userObjects;
    }

    /**
     * Used to check if the current user (remote user) has admin permission to the JIRA instance.
     * 
     * @return True if the user has permissions, false otherwise.
     */
    protected boolean hasAdminPermission()
    {
    	return ComponentManager.getInstance().getPermissionManager().hasPermission(
    			Permissions.ADMINISTER,
    			ComponentManager.getInstance().getJiraAuthenticationContext().getLoggedInUser());
    }

    /**
     * Used to check if the current user (remote user) has project admin permission to the project the component belongs to.
     * 
     * @return True if the user has permissions, false otherwise.
     */
    protected boolean hasProjectAdminPermission()
    {
    	return ComponentManager.getInstance().getPermissionManager().hasPermission(
    			Permissions.PROJECT_ADMIN,
    			this.getProject(),
    			ComponentManager.getInstance().getJiraAuthenticationContext().getLoggedInUser());
    }

    /**
     * Sets the project ID.  This is set via the velocity template to the current project being accessed.
     * 
     * @param pid The ID of the project.
     */
    public void setProjectId(Long pid)
    {
        this.pid = pid;
        this.setSelectedProjectId(this.pid);
    }
}