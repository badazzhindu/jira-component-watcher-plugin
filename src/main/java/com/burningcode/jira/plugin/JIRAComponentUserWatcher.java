/* Copyright (c) 2008, 2009, Ray Barham
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Ray Barham ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Ray Barham BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.burningcode.jira.plugin;

import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import org.ofbiz.core.entity.GenericEntityException;

import webwork.action.ActionContext;

import com.atlassian.jira.bc.EntityNotFoundException;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.component.multiuserpicker.UserPickerWebComponent;
import com.atlassian.crowd.embedded.api.User;

/**
 * Used to access and modify user watchers for a component.
 * 
 * This is the class that handles the retrieving, adding, and removing of
 * users as watchers for the JIRA Component Watcher plug-in.
 * 
 * @author Ray Barham
 */
public class JIRAComponentUserWatcher extends JIRAComponentAbstractWatcher<User>
{
    private static final long serialVersionUID = -7117983532714470392L;
    private final UserManager userManager;
    
    /**
     * Default constructor for the class. 
     * @throws GenericEntityException 
     */
    public JIRAComponentUserWatcher(UserManager userManager) throws GenericEntityException
    {
        super("componentUserWatcher", "username");
        this.userManager = userManager;
    }

    /**
     * Used to return a User object from the passed username.  This method is required and is called by methods
     * inherited from the JIRAComponentAbstractWatcher class.
     * 
     * @param watcher The username of the watcher.
     * @return User object
     */
    protected User castWatcher(String watcher)
    {
    	User user = userManager.getUserObject(watcher);
    	
    	// Remove the watcher from the component watcher if it no longer exists.
    	if(user == null && this.getPropertySet().exists(watcher))
    		this.getPropertySet().remove(watcher);

    	return user;
    }

    /**
     * Used to cast a list of usernames to a list of User objects.  This method is required and is called by methods
     * inherited from the JIRAComponentAbstractWatcher class.
     * 
     * @param watcherList List of usernames as GenericValues.
     * @return List of User objects.
     */
    protected Collection<User> castWatchers(Collection<String> watcherList)
    {
        Vector<User> castWatchers = new Vector<User>();

        for(Iterator<String> i = watcherList.iterator(); i.hasNext();)
        {
            String userName = (String)i.next();
            User user = (User)this.castWatcher(userName);

            if(user != null)
            {
                castWatchers.add(user);
            }
        }

        return castWatchers;
    }

    /**
     * Called when users are requested to be added as watchers.
     * 
     * @return The status of the execution.  "Success" is always returned unless the remote user does not have permissions which "securitybreach" is returned instead
     * @throws DataAccessException
     * @throws EntityNotFoundException
     */
	public String doAddWatchers() throws DataAccessException, EntityNotFoundException
    {
        if(!this.hasPermissions())
            return "securitybreach";

        @SuppressWarnings("unchecked")
		Collection<String> userNamesToAdd = UserPickerWebComponent.getUserNamesToAdd(this.getWatcherField());

        this.addWatchers(userNamesToAdd);

        return SUCCESS;
    }

    /**
     * Called when users are requested to be removed as watchers.
     * 
     * @return The status of the execution.  "Success" is always returned unless the remote user does not have permissions which "securitybreach" is returned instead
     * @throws DataAccessException
     * @throws EntityNotFoundException
     */
	public String doRemoveWatchers() throws DataAccessException, EntityNotFoundException
    {
        if(!this.hasPermissions())
            return "securitybreach";

        @SuppressWarnings("unchecked")
		Vector<String> userNamesToRemove = new Vector<String>(UserPickerWebComponent.getUserNamesToRemove(ActionContext.getParameters(), REMOVE_WATCHER_PREFIX));

        this.removeWatchers(userNamesToRemove);

        return SUCCESS;
    }
}