package com.burningcode.jira.notification;

import java.util.Map;

import com.atlassian.jira.notification.NotificationType;
import com.atlassian.jira.notification.NotificationTypeManager;
import com.burningcode.jira.notification.type.ComponentWatcherNotificationType;

public class ComponentWatcherNotificationTypeManager extends NotificationTypeManager {
	@Override
	public void setSchemeTypes(Map<String, NotificationType> schemeType) {
		schemeType.put("Component_Watchers", new ComponentWatcherNotificationType());
		super.setSchemeTypes(schemeType);
	}
}
